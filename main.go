package main

import (
	"fmt"
	"golang.org/x/net/proxy"
	"net"
	"net/http"
	"telegram-svc/core/services/web_scraper"
	"telegram-svc/message_handler"
	"time"
)

func main() {
	//server.Init()
	//return
	//func() {
	//	cert := &x509.Certificate{
	//		SerialNumber: big.NewInt(1658),
	//		Subject: pkix.Name{
	//			Organization:  []string{"ORGANIZATION_NAME"},
	//			Country:       []string{"COUNTRY_CODE"},
	//			Province:      []string{"PROVINCE"},
	//			Locality:      []string{"CITY"},
	//			StreetAddress: []string{"ADDRESS"},
	//			PostalCode:    []string{"POSTAL_CODE"},
	//		},
	//		NotBefore:    time.Now(),
	//		NotAfter:     time.Now().AddDate(10, 0, 0),
	//		SubjectKeyId: []byte{1, 2, 3, 4, 6},
	//		ExtKeyUsage:  []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
	//		KeyUsage:     x509.KeyUsageDigitalSignature,
	//	}
	//	priv, _ := rsa.GenerateKey(rand.Reader, 2048)
	//	pub := &priv.PublicKey
	//
	//	// Sign the certificate
	//	certificate, _ := x509.CreateCertificate(rand.Reader, cert, cert, pub, priv)
	//
	//	certBytes := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: certificate})
	//	keyBytes := pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(priv)})
	//
	//	// Generate a key pair from your pem-encoded cert and key ([]byte).
	//	x509Cert, _ := tls.X509KeyPair(certBytes, keyBytes)
	//
	//	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
	//		fmt.Println("hello")
	//	})
	//	tlsConfig := &tls.Config{
	//		Certificates: []tls.Certificate{x509Cert}}
	//	server := http.Server{Addr: ":8443", TLSConfig: tlsConfig}
	//
	//	glog.Fatal(server.ListenAndServeTLS("", ""))
	//}()

	go web_scraper.Selenium{}.New()
	//server.Init()
	//client := getProxyClient("192.168.1.6", "8000")
	//req, _ := http.NewRequest(http.MethodGet, "https://my.telegram.org/apps", nil)
	//do, err := client.Do(req)
	//fmt.Println(do, err)

	message_handler.StartBot(nil)
	//
	//
}

func getProxyClient(proxyIP, proxyPort string, auth ...*proxy.Auth) *http.Client {

	proxyurl := proxyIP + ":" + proxyPort
	var author *proxy.Auth = nil
	if auth != nil {
		author = auth[0]
	}
	dialer, err := proxy.SOCKS5("tcp", proxyurl, author,
		&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		},
	)

	if err != nil {
		fmt.Println(err)
		return nil
	}

	transport := &http.Transport{
		Proxy:               nil,
		Dial:                dialer.Dial,
		TLSHandshakeTimeout: 10 * time.Second,
	}

	return &http.Client{Transport: transport}
}
