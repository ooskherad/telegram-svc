package message_handler

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
)

func CommandHandler(bot *tgbotapi.BotAPI, update tgbotapi.Update) {
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")

	switch update.Message.Command() {
	case "start":
		msg.ReplyMarkup = mainKeyboard
		msg.Text = "خوش آمدید"
		if _, err := bot.Send(msg); err != nil {
			log.Panic(err)
		}
	default:
		msg.Text = ""
	}

}
