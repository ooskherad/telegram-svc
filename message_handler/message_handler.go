package message_handler

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
)

func MessageHandler(bot *tgbotapi.BotAPI, update tgbotapi.Update, updates tgbotapi.UpdatesChannel) {
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")

	switch update.Message.Text {
	case "تبنگو":
		//tabangoMassage.TabangooHandler(bot, update, updates)
		msg.Text = "خروج از تبنگو"
		msg.ReplyMarkup = mainKeyboard
	case "سبدگردان":
		msg.Text = "این قسمت در دست ساخت می باشد. لطفا صبور باشید ..."
	}
	if msg.Text != "" {
		if _, err := bot.Send(msg); err != nil {
			log.Panic(err)
		}
	}

}
