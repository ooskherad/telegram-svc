package message_handler

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
	"net/http"
)

func StartBot(client *http.Client) {
	//bot, err := tgbotapi.NewBotAPIWithClient("5637461086:AAGwyjFHlN9XOLJ_qiMQY4KmVxl2-DOEJj4", tgbotapi.APIEndpoint, client)
	bot, err := tgbotapi.NewBotAPI("5637461086:AAGwyjFHlN9XOLJ_qiMQY4KmVxl2-DOEJj4")
	if err != nil {
		log.Println(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)
	go func() {
		err := http.ListenAndServeTLS("0.0.0.0:8443", "certificate.crt", "privateKey.key", nil)
		if err != nil {
			fmt.Println(err)
		}
	}()
	//u := tgbotapi.NewUpdate(0)
	//u.Timeout = 60
	wh, err := tgbotapi.NewWebhookWithCert("https://31.57.215.154:8443/"+bot.Token, tgbotapi.FilePath("certificate.crt"))
	fmt.Println(err)

	_, err = bot.Request(wh)
	if err != nil {
		log.Fatal(err)
	}
	info, err := bot.GetWebhookInfo()
	if err != nil {
		log.Fatal(err)
	}

	if info.LastErrorDate != 0 {
		log.Printf("Telegram callback failed: %s", info.LastErrorMessage)
	}

	updates := bot.ListenForWebhook("/" + bot.Token)

	for update := range updates {
		log.Printf("%+v\n", update)
	}

	//updates := bot.GetUpdatesChan(u)
	//
	//for update := range updates {
	//	if update.Message == nil {
	//		continue
	//	}
	//	if update.Message.IsCommand() {
	//		CommandHandler(bot, update)
	//	} else if update.Message != nil {
	//		MessageHandler(bot, update, updates)
	//	}
	//
	//}
}
