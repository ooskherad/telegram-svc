package server

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

func NewRouter() *gin.Engine {
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	//add routers
	group := router.Group("/")
	group.GET("", func(context *gin.Context) {
		fmt.Println("sfd")
	})

	return router

}
