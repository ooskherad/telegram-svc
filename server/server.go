package server

import (
	"fmt"
	"telegram-svc/core/config"
)

func Init() {
	port := config.Conf.ServerPort
	r := NewRouter()
	err := r.RunTLS(fmt.Sprintf("0.0.0.0%s", port), "certificate.crt", "privateKey.key")
	//err := r.Run(fmt.Sprintf("0.0.0.0%s", port))
	if err != nil {
		return
	}
}
