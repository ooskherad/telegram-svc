package stock_info

import (
	"context"
	"telegram-svc/microservices/tabangoo"
	stockProto "telegram-svc/microservices/tabangoo/proto/stock_info"
)

type stockInfoServices struct {
	Client stockProto.StockServiceClient
}

func NewStockInfoServices() *stockInfoServices {
	return &stockInfoServices{Client: stockProto.NewStockServiceClient(tabangoo.ClientConnection)}
}

func (c *stockInfoServices) GetStockInfo(symbolName string) (*stockProto.StockInfoResponse, error) {
	req := &stockProto.StockInfoRequest{
		SymbolName: symbolName,
	}

	return c.Client.GetStockInfo(context.Background(), req)
}
