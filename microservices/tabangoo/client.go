package tabangoo

import (
	"google.golang.org/grpc"
	"log"
	"telegram-svc/core/config"
)

var ClientConnection *grpc.ClientConn

func init() {
	clientConnection, err := grpc.Dial(config.Conf.TabangooSvcUrl, grpc.WithInsecure(), grpc.WithBlock())

	if err != nil {
		log.Panicln("Could not connect:", err)
	}
	ClientConnection = clientConnection
}
