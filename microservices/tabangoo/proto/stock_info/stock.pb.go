// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.21.2
// source: stock_info/stock.proto

package stock_info

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type StockInfoRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	SymbolName string `protobuf:"bytes,1,opt,name=SymbolName,proto3" json:"SymbolName,omitempty"`
}

func (x *StockInfoRequest) Reset() {
	*x = StockInfoRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_stock_info_stock_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StockInfoRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StockInfoRequest) ProtoMessage() {}

func (x *StockInfoRequest) ProtoReflect() protoreflect.Message {
	mi := &file_stock_info_stock_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StockInfoRequest.ProtoReflect.Descriptor instead.
func (*StockInfoRequest) Descriptor() ([]byte, []int) {
	return file_stock_info_stock_proto_rawDescGZIP(), []int{0}
}

func (x *StockInfoRequest) GetSymbolName() string {
	if x != nil {
		return x.SymbolName
	}
	return ""
}

type StockInfoResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Identifier string `protobuf:"bytes,1,opt,name=Identifier,proto3" json:"Identifier,omitempty"`
}

func (x *StockInfoResponse) Reset() {
	*x = StockInfoResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_stock_info_stock_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StockInfoResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StockInfoResponse) ProtoMessage() {}

func (x *StockInfoResponse) ProtoReflect() protoreflect.Message {
	mi := &file_stock_info_stock_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StockInfoResponse.ProtoReflect.Descriptor instead.
func (*StockInfoResponse) Descriptor() ([]byte, []int) {
	return file_stock_info_stock_proto_rawDescGZIP(), []int{1}
}

func (x *StockInfoResponse) GetIdentifier() string {
	if x != nil {
		return x.Identifier
	}
	return ""
}

var File_stock_info_stock_proto protoreflect.FileDescriptor

var file_stock_info_stock_proto_rawDesc = []byte{
	0x0a, 0x16, 0x73, 0x74, 0x6f, 0x63, 0x6b, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x2f, 0x73, 0x74, 0x6f,
	0x63, 0x6b, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0a, 0x73, 0x74, 0x6f, 0x63, 0x6b, 0x5f,
	0x69, 0x6e, 0x66, 0x6f, 0x22, 0x32, 0x0a, 0x10, 0x53, 0x74, 0x6f, 0x63, 0x6b, 0x49, 0x6e, 0x66,
	0x6f, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1e, 0x0a, 0x0a, 0x53, 0x79, 0x6d, 0x62,
	0x6f, 0x6c, 0x4e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x53, 0x79,
	0x6d, 0x62, 0x6f, 0x6c, 0x4e, 0x61, 0x6d, 0x65, 0x22, 0x33, 0x0a, 0x11, 0x53, 0x74, 0x6f, 0x63,
	0x6b, 0x49, 0x6e, 0x66, 0x6f, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x1e, 0x0a,
	0x0a, 0x49, 0x64, 0x65, 0x6e, 0x74, 0x69, 0x66, 0x69, 0x65, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0a, 0x49, 0x64, 0x65, 0x6e, 0x74, 0x69, 0x66, 0x69, 0x65, 0x72, 0x32, 0x5d, 0x0a,
	0x0c, 0x53, 0x74, 0x6f, 0x63, 0x6b, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x4d, 0x0a,
	0x0c, 0x47, 0x65, 0x74, 0x53, 0x74, 0x6f, 0x63, 0x6b, 0x49, 0x6e, 0x66, 0x6f, 0x12, 0x1c, 0x2e,
	0x73, 0x74, 0x6f, 0x63, 0x6b, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x2e, 0x53, 0x74, 0x6f, 0x63, 0x6b,
	0x49, 0x6e, 0x66, 0x6f, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x1d, 0x2e, 0x73, 0x74,
	0x6f, 0x63, 0x6b, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x2e, 0x53, 0x74, 0x6f, 0x63, 0x6b, 0x49, 0x6e,
	0x66, 0x6f, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x42, 0x3e, 0x5a, 0x3c,
	0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x5f, 0x68, 0x61, 0x6e, 0x64, 0x6c, 0x65, 0x72, 0x2f,
	0x74, 0x65, 0x6c, 0x65, 0x67, 0x72, 0x61, 0x6d, 0x2f, 0x70, 0x61, 0x63, 0x6b, 0x61, 0x67, 0x65,
	0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x73, 0x74, 0x6f, 0x63, 0x6b, 0x5f, 0x69, 0x6e, 0x66,
	0x6f, 0x3b, 0x73, 0x74, 0x6f, 0x63, 0x6b, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x62, 0x06, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_stock_info_stock_proto_rawDescOnce sync.Once
	file_stock_info_stock_proto_rawDescData = file_stock_info_stock_proto_rawDesc
)

func file_stock_info_stock_proto_rawDescGZIP() []byte {
	file_stock_info_stock_proto_rawDescOnce.Do(func() {
		file_stock_info_stock_proto_rawDescData = protoimpl.X.CompressGZIP(file_stock_info_stock_proto_rawDescData)
	})
	return file_stock_info_stock_proto_rawDescData
}

var file_stock_info_stock_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_stock_info_stock_proto_goTypes = []interface{}{
	(*StockInfoRequest)(nil),  // 0: stock_info.StockInfoRequest
	(*StockInfoResponse)(nil), // 1: stock_info.StockInfoResponse
}
var file_stock_info_stock_proto_depIdxs = []int32{
	0, // 0: stock_info.StockService.GetStockInfo:input_type -> stock_info.StockInfoRequest
	1, // 1: stock_info.StockService.GetStockInfo:output_type -> stock_info.StockInfoResponse
	1, // [1:2] is the sub-list for method output_type
	0, // [0:1] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_stock_info_stock_proto_init() }
func file_stock_info_stock_proto_init() {
	if File_stock_info_stock_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_stock_info_stock_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StockInfoRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_stock_info_stock_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StockInfoResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_stock_info_stock_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_stock_info_stock_proto_goTypes,
		DependencyIndexes: file_stock_info_stock_proto_depIdxs,
		MessageInfos:      file_stock_info_stock_proto_msgTypes,
	}.Build()
	File_stock_info_stock_proto = out.File
	file_stock_info_stock_proto_rawDesc = nil
	file_stock_info_stock_proto_goTypes = nil
	file_stock_info_stock_proto_depIdxs = nil
}
