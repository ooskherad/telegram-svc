package message_handler

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

const (
	returnText = "بازگشت"
	tableText  = "تابلو سهام"
)

var (
	TabangoMainKeyboard = tgbotapi.NewReplyKeyboard(
		tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton(tableText),
			tgbotapi.NewKeyboardButton("سایت ما"),
		),
		tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton(returnText),
		),
	)
)
