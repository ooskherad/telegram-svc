package message_handler

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
)

func TabangooHandler(bot *tgbotapi.BotAPI, message tgbotapi.Update, updates tgbotapi.UpdatesChannel) {
	msg := tgbotapi.NewMessage(message.Message.Chat.ID, message.Message.Text)
	msg.ReplyMarkup = TabangoMainKeyboard
	if _, err := bot.Send(msg); err != nil {
		log.Panic(err)
	}

	for update := range updates {
		fmt.Println(update)
		if update.Message.Text == returnText {
			return
		}

		if update.Message != nil {
			MessageHandler(bot, update)
		}
		//
		//	if update.Message.IsCommand() {
		//		CommandHandler(bot, update)
		//	}
		//
	}
}
