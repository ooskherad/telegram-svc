package message_handler

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
	"telegram-svc/core/services/web_scraper"
	"telegram-svc/microservices/tabangoo/services/stock_info"
)

func MessageHandler(bot *tgbotapi.BotAPI, update tgbotapi.Update) {
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")

	switch update.Message.Text {
	case tableText:
		msg.Text = "به تابلو خوش آمدید. اینجا هیچ کاری نمیکنه"
	default:
		getStockTable(bot, update, update.Message.Text)
	}
	if msg.Text != "" {
		if _, err := bot.Send(msg); err != nil {
			log.Panic(err)
		}
	}

}

func getStockTable(bot *tgbotapi.BotAPI, update tgbotapi.Update, symbol string) {
	svc := stock_info.NewStockInfoServices()
	s := web_scraper.Service

	msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")
	info, _ := svc.GetStockInfo(symbol)
	if info.GetIdentifier() != "" {
		s.Get("https://stackoverflow.com/questions/61138983/telegram-bot-returning-null")
		s.Crawl("#question > div.post-layout > div.postcell.post-layout--right > div.s-prose.js-post-body > pre")
		screenShot, err := s.TakeScreenShot()
		if err != nil {
			msg.Text = "مشکلی رخ داده است لطفا مجدد تلاش کنید"
			return
		}
		photo := tgbotapi.NewInputMediaPhoto(tgbotapi.FileBytes{Name: "image.jpg", Bytes: screenShot})
		mediaGroup := tgbotapi.NewMediaGroup(update.Message.Chat.ID, []interface{}{photo})
		if _, err := bot.SendMediaGroup(mediaGroup); err != nil {
			log.Panic(err)
		}
	}
	return
}
