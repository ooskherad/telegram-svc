package web_scraper

import (
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
	"os"
)

type Selenium struct {
	service     *selenium.Service
	driver      selenium.WebDriver
	selectedDiv selenium.WebElement
}

var Service *Selenium

func (s Selenium) New() (*Selenium, error) {
	service, err := selenium.NewChromeDriverService("core/drivers/chromedriver", 4444)
	if err != nil {
		return nil, err
	}
	caps := selenium.Capabilities{}
	caps.AddChrome(chrome.Capabilities{Args: []string{
		"window-size=1920x1080",
		"--no-sandbox",
		"--disable-dev-shm-usage",
		"disable-gpu",
		"--headless",
	}})

	driver, err := selenium.NewRemote(caps, "")
	if err != nil {
		return nil, err
	}

	Service = &Selenium{driver: driver, service: service}
	return &Selenium{driver: driver, service: service}, nil
}

func (s Selenium) stop() error {
	err := s.service.Stop()
	if err != nil {
		return err
	}
	return nil
}

func (s *Selenium) Crawl(selector string) (*Selenium, error) {
	outputDiv, err := s.driver.FindElement(selenium.ByCSSSelector, selector)
	if err != nil {
		return nil, err
	}
	s.selectedDiv = outputDiv
	return s, nil
}

func (s Selenium) Get(url string) error {
	err := s.driver.Get(url)
	if err != nil {
		return err
	}
	return nil
}

func (s Selenium) TakeScreenShot() ([]byte, error) {
	var screenshot []byte
	var err error
	if s.selectedDiv != nil {
		screenshot, err = s.selectedDiv.Screenshot(false)
	} else {
		screenshot, err = s.driver.Screenshot()
	}

	if err != nil {
		return nil, err
	}
	return screenshot, nil
}

func SaveImage(name string, image []byte) {
	out, err := os.Create(name + ".jpg")
	if err != nil {
		panic(err)
	}
	defer out.Close()

	_, err = out.Write(image)
	if err != nil {
		panic(err)
	}
}
