package database

import (
	"fmt"
	"gorm.io/gorm"
	"log"
	"reflect"
)

type Repository interface {
	Select()
	Table()
	Db()
}

type RepositoryModel struct {
	Model     interface{}
	TableName string
}

func (repositoryModel RepositoryModel) Db() *gorm.DB {
	db, err := GetDatabaseConnection()
	if err != nil {
		log.Panicln(err)
	}
	return db
}

func (repositoryModel RepositoryModel) Table() *gorm.DB {
	return repositoryModel.Db().Table(repositoryModel.TableName)
}

// With relation with preload
func (repositoryModel RepositoryModel) With(tx interface{}, relations ...string) *gorm.DB {
	statement := repositoryModel.Db()
	for _, relation := range relations {
		statement = statement.Preload(relation)
	}
	return statement.Find(tx)
}

func (repositoryModel RepositoryModel) All(tx interface{}) *gorm.DB {
	return repositoryModel.Db().Find(tx)
}

func (repositoryModel RepositoryModel) AllWithDeleted(tx interface{}) *gorm.DB {
	return repositoryModel.Db().Unscoped().Find(tx)
}

func (repositoryModel RepositoryModel) DeletedFiled(tx interface{}) *gorm.DB {
	return repositoryModel.Db().Where("deleted_at ").Find(tx)
}

func (repositoryModel RepositoryModel) Create(tx interface{}) *gorm.DB {
	return repositoryModel.Db().Create(tx)
}

// Where not implement yet !!!!
func (repositoryModel RepositoryModel) Where(tx interface{}) {
	val := reflect.ValueOf(tx)
	//whereClause := ""
	indirect := reflect.Indirect(val)
	for i := 0; i < val.NumField(); i++ {
		//whereClause += fmt.Sprintf("%s = %s ", indirect.Type().Field(i).Name, indirect.Field(i))
		fieldName := indirect.Type().Field(i).Name
		fieldValue := indirect.FieldByName(fieldName)
		fieldType := indirect.Type().Field(i).Type.Kind().String()
		if fieldType == "uint" {
			fmt.Println(fieldName, fieldValue.Uint())
			//	} else if fieldType == "string" {
			//		fmt.Println(fieldName, fieldValue.String())
			//	} else if utils.IsIn([]string{"int", "uint", "int32", "int16", "int8"}, fieldType) {
			//		fmt.Println(fieldName, fieldValue.Int())
			//
			//	} else if utils.IsIn([]string{"float32", "float64"}, fieldType) {
			//		fmt.Println(fieldName, fieldValue.Float())
			//	} else if fieldName[len(fieldName)-2:] == "At" {
			//		fmt.Println(fieldName, reflect.TypeOf(fieldType), fieldValue.Interface().(data_types.Date))
		}
	}
	fmt.Println()
}
