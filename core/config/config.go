package config

import (
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	"log"
)

type Config struct {
	// dataBase Configs
	DataBasePort     string `mapstructure:"DATABASE_PORT"`
	DataBaseUserName string `mapstructure:"DATABASE_USERNAME"`
	DataBasePassword string `mapstructure:"DATABASE_PASSWORD"`
	DataBaseHost     string `mapstructure:"DATABASE_HOST"`
	DataBaseName     string `mapstructure:"DATABASE_NAME"`
	// server Configs
	ServerPort string `mapstructure:"SERVER_PORT"`
	//tabangoo-svc
	TabangooSvcUrl string `mapstructure:"TABANGOO_SERVICE_URL"`
}

var Conf Config

func init() {
	Conf = LoadConfig()
}

func LoadConfig() (envVariables Config) {
	setPostgresDefault()

	viper.AddConfigPath("./core/config/envs")
	viper.SetConfigName(".env")
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	var err error
	if err = viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Println("No .env file found! if you have .env put it under core/config/envs/.env")
		} else {
			log.Println("Config file found but encountered another error:")
			log.Println(err.Error())
		}
	}

	viper.OnConfigChange(func(e fsnotify.Event) {
		log.Println("Config file changed:", e.Name)
	})
	viper.WatchConfig()

	err = viper.Unmarshal(&envVariables)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}
	return
}

func setPostgresDefault() {
	viper.SetDefault("PORT", "50051")
	viper.SetDefault("DATABASE_PORT", "5432")
	viper.SetDefault("DATABASE_USERNAME", "javad")
	viper.SetDefault("DATABASE_HOST", "141.11.184.176")
	viper.SetDefault("DATABASE_NAME", "tabango")
	viper.SetDefault("SERVER_PORT", "8000")
}
